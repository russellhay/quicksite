import os.path
import webassets

def _environment(data, parent, output_dir):
    environment = webassets.Environment()
    environment.directory = output_dir
    environment.load_path = [ parent ]
    environment.debug = data.get('debug', False)

    return environment

def _bundle(environment, data, filters, output_dir):
    for output_file,source_files in data.items():
        bundle = webassets.Bundle(
            *source_files,
            filters=filters,
            output=os.path.join(output_dir, output_file)
        )
        environment.register(output_file.replace(".", "_"), bundle)

def _build(environment):
    for bundle in environment:
        bundle.build()

def _has_sass(file_list):
    return any((".sass" in x for x in file_list))

def package_js(data, parent, output_dir):
    environment = _environment(data, parent, output_dir)
    _bundle(environment, data['js'], [], output_dir)
    _build(environment)
        

def package_css(data, parent, output_dir):
    environment = _environment(data, parent, output_dir)
    filters = []
    
    if any((_has_sass(x) for x in data['css'].values())):
        filters.append('sass')

    filters.append("cssmin")
    _bundle(environment, data['css'], filters, output_dir)
    _build(environment)
