**v0.1.1**

    * Fixed a small cross-platform path issue with configbuilder where it generates key names incorrectly.
