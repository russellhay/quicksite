from argparse import ArgumentParser
import glob
import json
import datetime
import os.path as path
import threading
from dateutil.rrule import rrule, MONTHLY, SA

import configbuilder.from_csv

output_file = "config.json"
configs_path = "configs{0}".format(path.sep)

def _ordinal_suffix(day):
    if 4 <= day <= 20 or 24 <= day <= 30:
        return "th"
    else:
        return ["st", "nd", "rd"][day % 10 - 1]


def _next_second_saturday():
    today = datetime.date.today()
    rule = rrule(MONTHLY, count=1, byweekday=SA(+2), dtstart=today)
    next_one = rule[0]
    return "".join((next_one.strftime("%B %d"),
                    "<span class=\"suffix\">",
                     _ordinal_suffix(next_one.day),
                    "</span>"
    ))


def generate():
    print "Generating config.json"
    data_parts = glob.glob("configs/*.csv")
    data = { }
    for part in data_parts:
        key = part.replace(configs_path, "").replace(".csv", "")
        data[key] = configbuilder.from_csv.main(part)

    boilerplate = json.load(open("configs/boiler.json"))
    data['next_artwalk'] = _next_second_saturday()  # Specific to buildingc
    boilerplate['data'] = data
    with open(output_file, "w") as f:
        json.dump(boilerplate, f, indent=2)
    print "Done"

def do(args):
    if not args.regen:
        generate()
        return

    from watchdog.observers import Observer
    from watchdog.events import FileSystemEventHandler
    import time

    class BatchCaller(object):
        """ Call a function in a background thread, but ignore all calls until the previous one has finished processing

        This is used to generate the site in the background.
        """
        def __init__(self, func):
            self._function = func
            self._call_pending = threading.Lock()

        def __call__(self):
            if not self._call_pending.acquire(False):
                return

            thread = threading.Thread(target=self._execute)
            thread.start()

        def _execute(self):
            try:
                self._function()
            finally:
                self._call_pending.release()


    class _RegenerateHandler(FileSystemEventHandler):
        """ Handle file system events """
        def __init__(self):
            super(_RegenerateHandler, self).__init__()
            self._generate = BatchCaller(generate)

        def on_any_event(self, event):
            filename = path.basename(event.src_path)
            self._generate()

    print "Watching configs directory..."
    handler = _RegenerateHandler()
    observer = Observer()
    observer.schedule(handler, "configs", recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


def args():
    parser = ArgumentParser()
    parser.add_argument("--regen", default=False, action="store_const", const=True,
                        help="wait and regenerate when config pieces change")

    return parser.parse_args()

def main():
    do(args())
if __name__ == "__main__":
    main()
