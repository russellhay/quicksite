# QuickSite

Quicksite is a *simple* and *feature-light* static site generator that tries to do very little.  It is json, csv, and jinja2 based.

## QuickSite Quick Start Guide

### Install

Clone using `git clone https://bitbucket.org/russellhay/quicksite.git` or `pip install QuickSite`

### Configuration

Create a directory for your project, and inside create a directories named `configs`, `static`, `templates`.

Inside of configs, put a json file named `boiler.json` with contents that looks like this:

    :::javascript
    {
      "static_dir": "{here}/static",
      "template_dir": "{here}/templates",
      "output_dir": "{here}/output",
      "templates": {
        "index.html": "index.html",
        "artists/{slug}/index.html": {
          "template": "artist.html",
          "type": "multi",
          "data_name": "artists"
        }
      }
    }

Then, inside of configs, create a csv file named `artists.csv`, this is a csv file, and can have anything in it that
you want.  The first line should be the header names of the file, and the rest should be entries.

The `boiler.json` specifies that two templates are needed, one named `index.html` and one named `artist.html`.

`index.html` is considered a single-page entry, so it generates a single file in the output directory.

`artist.html` is mapped to multiple files that fit into `artists/{slug}/index.html` where `{slug}` is expanded from the
`artists.csv` file.

So create two jinja2 templates in the `templates` directory, one named `index.html` and the other named `artist.html`.
All data in all of the csv files will be included and available to all templates.

All files in the `static` directory will be copied, verbatim, into the `output` directory.

### Generating your site

Generating your site is a two step process.  The first is to combine the csv files plus the boiler.json into a full
config file.

To do this, go into your project's main directory, and run the following command:

    :::bash
    configbuilder

Simple, the configbuilder assumes the `configs` directory contains all of the configuration files, including
`boiler.json`.  It generates `config.json` in the current working directory

Then do the following:

    :::bash
    quicksite config.json

This will generate the site into `output` (or whatever you have set in your `boiler.json` file as `output_dir`)

Done.